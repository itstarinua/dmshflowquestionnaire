<!DOCTYPE html>
<html>
	<head>		
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width"/>
		<meta content="DStaroselskiy (IT-Star company)" name="author" />
		<meta content="noindex,nofollow" name="Robots"/>		
		<title>*|MAIL_TITLE|*</title>
	</head>
	<body>
		<h1>*|MAIL_TITLE_DATE|*</h1>
		<hr>
		<h2>Данные пользователя</h2>
		<p><b>Имя:</b> *|USER_NAME|*</p>
		<p><b>E-MAIL:</b> *|USER_EMAIL|*</p>
		<p><b>Телефон:</b> *|USER_PHONE|*</p>
		<p><b>Название предприятия:</b> *|COMPANY|*</p>
		<p><b>Сфера деятельности:</b> *|FIELD_OF_ACTIVITY|*</p>
		<hr/>
		<h2>Данные опросника</h2>
		<table>
			<tbody>
				<tr class="characteristic">
					<td>Опишите свою систему водоподготовки</td>
					<td>*|the_water_treatment_system|*</td>
				</tr>
				<tr class="characteristic">
					<td>Добавьте узел, который есть у вас, но здесь не указан:</td>
					<td>*|mechanical_cleaning_other|*</td>
				</tr>
				<tr class="characteristic">
					<td>Суточное потребление холодной воды, м³:</td>
					<td>*|daily_consumption_of_cold_water|*</td>
				</tr>
				<tr class="characteristic">
					<td>Суточное потребление горячей воды, м³:</td>
					<td>*|daily_consumption_of_hot_water|*</td>
				</tr>
				<tr class="characteristic">
					<td>Жёсткость воды</td>
					<td>*|the_hardness_of_the_water|*</td>
				</tr>
				<tr class="characteristic">
					<td>Какая есть проблема?</td>
					<td>*|what_is_the_problem|*</td>
				</tr>
				<tr class="characteristic">
					<td colspan="2">Диаметр трубопровода подходящего к проблемному участку</td>
				</tr>
				<tr class="characteristic">
					<td>Накипь Диаметр</td>
					<td>*|scale|*</td>
				</tr>
				<tr class="characteristic">
					<td>Накипь Температура</td>
					<td>*|scale_t|*</td>
				</tr>
				<tr class="characteristic">
					<td>Бактерии Диаметр</td>
					<td>*|bacteria|*</td>
				</tr>
				<tr class="characteristic">
					<td>Бактерии Температура</td>
					<td>*|bacteria_t|*</td>
				</tr>
				<tr class="characteristic">
					<td>Жёсткость воды Диаметр</td>
					<td>*|jobromania|*</td>
				</tr>
				<tr class="characteristic">
					<td>Жёсткость воды Температура</td>
					<td>*|jobromania_t|*</td>
				</tr>
				<tr class="characteristic">
					<td>Какие применяются дезинфицирующие средства?</td>
					<td>*|what_sanitizers_are_used|*</td>
				</tr>
				<tr class="characteristic">
					<td>Дополнительная информация:</td>
					<td>*|additional_information|*</td>
				</tr>
			</tbody>
		</table>
		<hr/>
		<h6>Письмо сгенирировано автомапически плагином H-Flow Calculator</h6>
	</body>
</html>
