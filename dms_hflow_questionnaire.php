<?php
/*
Plugin Name: h-flow Questionnaire
Plugin URI: http://it-star.in.ua
Description: Опросник разработанный по требованию H-Flow. Для размещения на страницы используйте шорткод [dms-hflow-questionnaire] Для вызова создайте кнопку с классом .hflow_questionnaire_show
Version: 1.0.1
Author: DStaroselskiy
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
*/

define("HFLOW_QUESTIONNAIRE_DIR", plugin_dir_path( __FILE__ ), true);
define("HFLOW_QUESTIONNAIRE_URL", plugin_dir_url( __FILE__ ), true);

if( !class_exists( '\DStaroselskiy\HELLO' ) ) require_once( HFLOW_QUESTIONNAIRE_DIR."/include/class_HELLO.php");
if( !class_exists( '\DStaroselskiy\inc\ADMIN_NOTICES' ) ) require_once( HFLOW_QUESTIONNAIRE_DIR."/include/class_ADMIN_NOTICES.php");
if( !class_exists( '\DStaroselskiy\Plugins\HFLOW_QUESTIONNAIRE' ) ) require_once( HFLOW_QUESTIONNAIRE_DIR."/include/class_HFLOW_QUESTIONNAIRE.php");
if( !class_exists( '\DStaroselskiy\Shortcodes\SHORTCODE_HFLOW_QUESTIONNAIRE.php' ) ) require_once( HFLOW_QUESTIONNAIRE_DIR."/include/shortcodes/class_SHORTCODE_HFLOW_QUESTIONNAIRE.php");

global $DMS_h_flow_questionnaire;
$DMS_h_flow_questionnaire = new \DStaroselskiy\Plugins\HFLOW_QUESTIONNAIRE();

?>