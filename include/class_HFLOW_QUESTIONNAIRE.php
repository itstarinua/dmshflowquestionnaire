<?php
/***************************************************
Класc для реализации блока с социальными иконками
by DStaroselskiy 

Version: 0.1
Date: 2016-04-26
****************************************************/

namespace DStaroselskiy\Plugins;

class HFLOW_QUESTIONNAIRE extends \DStaroselskiy\inc\ADMIN_NOTICES {
	protected $plugin_version = "0.1";
	protected $plugin_name = "H-Flow QUESTIONNAIRE";
	protected $admin_page_id;	
	protected $options = array(
			"power_off" => 'false',
			"title" => '',
			"send_from" => '',
			"send_to" => '',
	);	
	
	public function get_options(){
		return $this->options;
	}
	
	protected $nonce_fild = "dms_nonce";
	protected $nonce_action = "dms-h-flow-questionnaire-action";
	
	
	public function add_admin_menu(){		
		global $admin_page_hooks;
        if ( isset( $admin_page_hooks['dms-hello'] ) ) {
			$this->admin_page_id = \add_submenu_page( 'dms-hello', __('Настройки опросника ГИДРОФЛОУ.','dms_plugin'), __('H-Flow Questionnaire','dms_plugin'), 'manage_options', 'dms-h-flow-questionnaire', array( &$this, 'show_admin_page' ) ); 
		}else{
			$this->admin_page_id = \add_menu_page( __('Настройки опросника ГИДРОФЛОУ.','dms_plugin'), __('H-Flow Questionnaire','dms_plugin'), 'manage_options', 'dms-h-flow-questionnaire', array( &$this, 'show_admin_page' ), SOCIAL_LINKS_URL.'images/Logotype-It-star-(finish)-25x25.png', 71 );
		}
	}
	
	public function show_admin_page() {
		
		if( !file_exists(HFLOW_QUESTIONNAIRE_DIR.'tpl/page-admin-option.tpl') ) {
			echo '<div class="wrap"><h1>',\get_admin_page_title(),'</h1><div>',__('Файл отображения настроек отсутствует.','dms_plugin'),'</div></div>';
			return false;
		}
		$page = file_get_contents(HFLOW_QUESTIONNAIRE_DIR.'tpl/page-admin-option.tpl');

		$option_page = str_replace(
			array(
				'*|PAGE_TITLE|*', 
				'*|WP_NONCE_FIELD|*', 
				'*|HFLOW_QUESTIONNAIRE_BEFORE|*', 
				'*|HFLOW_QUESTIONNAIRE_MAIL_BLOCK_TITLE|*', 				
				'*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TITLE|*', 
				'*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TRUE_CHECKED|*', 
				'*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_FALSE_CHECKED|*', 
				'*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TRUE_TEXT|*', 
				'*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_FALSE_TEXT|*', 				
				'*|HFLOW_QUESTIONNAIRE_MAIL_TITLE_TEXT|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_TITLE|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO_TEXT|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO_DESCRIPTION|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM_TEXT|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM|*',
				'*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM_DESCRIPTION|*',
				'*||*',
				'*|HFLOW_QUESTIONNAIRE_AFTER|*',
				'*|SUBMIT_BUTTON|*', 
			),
			array(
				\get_admin_page_title(),
				\wp_nonce_field( $this->nonce_action, $this->nonce_fild , true , false ),
				\apply_filters('admins_of_hflow_questionnaire_before',''),
				\esc_html__('Настройки отправки запроса:','dms_plugin'),
				\esc_html__('Состояние форму отправки запроса:','dms_plugin'),
				\checked( $this->options['power_off'], 'true', false ),
				\checked( $this->options['power_off'], 'false', false ),
				\esc_html__('Включить','dms_plugin'),
				\esc_html__('Выключить','dms_plugin'),
				\esc_html__('Заголовок письма:','dms_plugin'),
				$this->options["title"],
				\esc_html__('Email адрес получателей:','dms_plugin'),
				$this->options["send_to"],
				\esc_html__('Можно указывать несколько адресов через пробел, запятую, точку с запятой.','dms_plugin'),
				\esc_html__('Email адрес отправителя:','dms_plugin'),
				$this->options["send_from"],
				\esc_html__('Можно указывать только один адрес.','dms_plugin'),
				'',
				\apply_filters('admins_of_hflow_questionnaire_after',''),
				\get_submit_button( esc_html__('Сохранить изменения','dms_plugin'), 'primary'),
			),
			$page
		);	
		echo \apply_filters('print_option_page_of_hflow_questionnaire', $option_page);
	}
	
	
	public function set_new_options(){
		$current_screen = \get_current_screen();
		if( ( $this->admin_page_id != $current_screen->base)
			|| !isset($_POST[ $this->nonce_fild ])
		) return false;
		
		if( !(\current_user_can('manage_options') ) ){
			$this->add_admin_notices( esc_html__('Данные не сохранены. У Вас не достаточно прав для изменения параметров.' , 'dms_plugin'), 'error', 'message-current_user_can');
			return false;
		}
		
		
		if( !wp_verify_nonce($_POST[ $this->nonce_fild ], $this->nonce_action ) ) {
			$this->add_admin_notices( esc_html__('Данные не сохранены. Защитный код не верен. Обновите страницу и попробуйте еща раз.' , 'dms_plugin'), 'error', 'message-wp_verify_nonce');
			return false;
		}
		
		$is_error = false;
		
		if( isset($_POST["title"]) ){
			$this->options["title"] = esc_html($_POST["title"]);			
		}
		
		if( isset($_POST['power_off']) ){
			if( in_array($_POST['power_off'], array("true", "false"))){
				$this->options['power_off'] = $_POST['power_off'];
			}else{
				$is_error = true;
				$this->add_admin_notices( esc_html__('Не правильно выбрали "Состояние форму отправки запроса". Сделайте правильный выбор.','dms_plugin') , 'notice-warning', '');
			}
		}
		
		if( isset($_POST['send_to']) ){
			if( $_POST['send_to'] == "" ){
				$this->options['send_to'] = get_option("admin_email");;
			}else{				
				$mail_to_str = str_replace( array(" ","  ", ";", ", ", "; ", " , ", " ; ", " ,", " ;") , ',' , $_POST['send_to']);
				$mail_to = explode( "," , $mail_to_str);
				foreach($mail_to as $email ){
					if( !( \is_email($email) ) ) $this->add_admin_notices( esc_html( sprintf(__('E-mail получателя - %s указан не верно.','dms_plugin'), $email) ) , 'notice-warning', '');
				}
				$this->options['send_to'] = esc_sql($mail_to_str);
			}
		}

		if( isset($_POST['send_from']) ){
			if( $_POST['send_from'] == "" ){
				$this->options['send_from'] = get_option("admin_email");;
			}else{
				if( !( \is_email($_POST['send_from']) ) ) $this->add_admin_notices( esc_html( sprintf(__('E-mail отправителя - %s указан не верно.','dms_plugin'), $_POST['send_from']) ) , 'notice-warning', '');
				$this->options['send_from'] = $_POST['send_from'];
			}
		}
		

		if($is_error) {
			$this->add_admin_notices(  esc_html__('Данные не сохранены.','dms_plugin') , 'error', 'message-not-save');
		}else {
			$options = get_option('HFLOW_QUESTIONNAIRE');
			//if( count( array_diff_assoc($this->options, $options) ) > 0 ){
			if( $options !== $this->options) {
				if( \update_option( 'HFLOW_QUESTIONNAIRE', $this->options, 'true' ) ){
					$this->add_admin_notices(  esc_html__('Данные сохранены.','dms_plugin') , 'updated', 'message-save');			
				}else{
					$this->add_admin_notices(  esc_html__('Ошибка сохранения данных. Пожалуйста повторите еще раз.','dms_plugin') , 'error', 'message-not-save');
				}
			}else{
				$this->add_admin_notices(  esc_html__('Данные сохранены.','dms_plugin') , 'updated', 'message-save');
			}
		}
	}
	
	public function load_plugin_options(){				
		$this->options["title"] = esc_html__('Заполнен опросник.','dms_plugin');
		$this->options["send_from"] = $this->options["send_to"] = get_option("admin_email");
		$this->options = wp_parse_args( \get_option('HFLOW_QUESTIONNAIRE'),$this->options );
		return $this->options;
	}
	

	function __construct() {
		parent::__construct();
		\add_action('admin_head', array( &$this, 'admin_notices_init'),100,0 );
		\add_action('admin_menu', array( &$this, 'add_admin_menu'),20,0 );
		\add_action('current_screen', array( &$this, 'set_new_options'), 21, 0);
		\add_action('init', array( &$this, 'load_plugin_options'), 20, 0);
	}
}
?>