<div class="wrap">
	<h1>*|PAGE_TITLE|*</h1>
	<form method="post" enctype="multipart/form-data" action="" id="">
		*|WP_NONCE_FIELD|*
		<table class="form-table">
			<tbody>
				*|HFLOW_QUESTIONNAIRE_BEFORE|*
				<tr>
					<th scope="row" colspan="2">
						<h2>*|HFLOW_QUESTIONNAIRE_MAIL_BLOCK_TITLE|*</h2>
					</th>
				</tr>
				<tr>
					<th scope="row" id="hflow-QUESTIONNAIRE-mail-power-off-title">
						*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TITLE|*
					</th>
					<td>
						<label><input name="power_off" id="hflow-QUESTIONNAIRE-mail-power-off-true" value="true" *|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TRUE_CHECKED|* class="" type="radio">*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_TRUE_TEXT|*</label><br>
						<label><input name="power_off" id="hflow-QUESTIONNAIRE-mail-power-off-false" value="false" *|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_FALSE_CHECKED|* class="" type="radio">*|HFLOW_QUESTIONNAIRE_MAIL_POWER_OFF_FALSE_TEXT|*</label>
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-QUESTIONNAIRE-mail-title">
						*|HFLOW_QUESTIONNAIRE_MAIL_TITLE_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="title" id="mail-title" value="*|HFLOW_QUESTIONNAIRE_MAIL_TITLE|*" type="text">
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-QUESTIONNAIRE-mail-send-to-title">
						*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="send_to" id="hflow-QUESTIONNAIRE-mail-send-to" value="*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO|*" type="text"></br>
						<p>*|HFLOW_QUESTIONNAIRE_MAIL_SEND_TO_DESCRIPTION|*</p>
					</td>
				</tr>
				<tr>
					<th scope="row" id="hflow-QUESTIONNAIRE-mail-send-from-title">
						*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM_TEXT|*
					</th>
					<td>
						<input style="width: 100%;" name="send_from" id="hflow-QUESTIONNAIRE-mail-send-from" value="*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM|*" type="text"></br>
						<p>*|HFLOW_QUESTIONNAIRE_MAIL_SEND_FROM_DESCRIPTION|*</p>
					</td>
				</tr>
				*|HFLOW_QUESTIONNAIRE_AFTER|*
			</tbody>
		</table>	
		*|SUBMIT_BUTTON|*
	</form>
</div>