<?php
/***************************************************
Клас для отображения в админке верхнего уровня меню 
для всех страниц настроек плагинов и тем разработанных 
by DStaroselskiy 

Version: 0.1
Date: 2016-04-21
****************************************************/

namespace DStaroselskiy;

if( !class_exists( 'DStaroselskiy\HELLO' ) ) {
	class HELLO {
		private $class_name = "DStaroselskiy\HELLO";
		private $class_vercion = "0.1";
		
		public function add_admin_menu(){		
			global $admin_page_hooks;
			if ( !isset( $admin_page_hooks['dms-hello'] ) ) {
				add_menu_page( __('Здравствуйте, Я DStaroselskiy.','dms_plugin'), __('DStaroselskiy','dms_plugin'), 'manage_options', 'dms-hello', array( &$this, 'show_admin_page' ), plugin_dir_url( dirname(__FILE__) ).'images/Logotype-It-star-(finish)-25x25.png', 71 );
			}
		}
		
		public function show_admin_page(){
			
		}
		
		function __construct() {
			add_action('admin_menu', array( &$this, 'add_admin_menu'), 1); //Добавляем в админ-меню пункты управления плагином
			
		}
	};
	
	global $DMS_admin_hello_page;
	$DMS_admin_hello_page = new HELLO();
}

?>