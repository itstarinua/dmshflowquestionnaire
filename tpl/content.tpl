<form id="hflow_questionnaire" class="dms-hidden" action="" method="POST">
	*|WP_NONCE_FIELD|*
	*|HFLOW_QUESTIONNAIRE_BEFORE|*
	<div class="questionnaire-section" >
		<div id="questionnaire-section-01">
			<h3>Опишите свою систему водоподготовки</h3>
			<ul>
				<li>
					<label for="mechanical_cleaning">Механическая очистка</label>
					<input type="checkbox" id="mechanical_cleaning" name="the_water_treatment_system" value="Механическая очистка">
				</li>
				<li>
					<label for="the_removal_of_iron">Удаление железа</label>
					<input type="checkbox" id="the_removal_of_iron" name="the_water_treatment_system" value="Удаление железа">
				</li>
				<li>
					<label for="the_removal_of_hardness_salts">Удаление солей жёсткости</label>
					<input type="checkbox" id="the_removal_of_hardness_salts" name="the_water_treatment_system" value="Удаление солей жёсткости">
				</li>
				<li>
					<label for="carbon_filter">Угольный фильтр</label>
					<input type="checkbox" id="carbon_filter" name="the_water_treatment_system" value="Угольный фильтр">
				</li>
				<li>
					<label for="uv_lamp">УФ лампа</label>
					<input type="checkbox" id="uv_lamp" name="the_water_treatment_system" value="УФ лампа">
				</li>
			</ul>
			<div>
				<label for="mechanical_cleaning_other">Добавьте узел, который есть у вас, но здесь не указан:</label>
				<input type="text" id="mechanical_cleaning_other" name="mechanical_cleaning_other" value="">
			</div>
			<div class="dms-col-6">
				<label for="daily_consumption_of_cold_water">Суточное потребление холодной воды, м³:</label>
				<input type="text" id="daily_consumption_of_cold_water" name="daily_consumption_of_cold_water" value="">
			</div>
			<div class="dms-col-6">
				<label for="the_hardness_of_the_water">Жёсткость воды</label>
				<input type="text" id="the_hardness_of_the_water" name="the_hardness_of_the_water" value="">
				<span>мг-экв/л</span>
			</div>
			<div class="dms-col-6">
				<label for="daily_consumption_of_hot_water">Суточное потребление горячей воды, м³:</label>
				<input type="text" id="daily_consumption_of_hot_water" name="daily_consumption_of_hot_water" value="">
			</div>
			<div class="dms-nav">
				<button type="button" class="hflow_questionnaire-next">Продолжить</button>
			</div>
		</div>
	</div>
	<div class="questionnaire-section dms-hidden" >
		<div id="questionnaire-section-02">
			<h3>Какая есть проблема?</h3>
			<ul>
				<li>
					<label for="Scale">Накипь</label>
					<input type="checkbox" id="Scale" name="what_is_the_problem" value="Накипь">
				</li>
				<li>
					<label for="Bacteria">Бактерии</label>
					<input type="checkbox" id="Bacteria" name="what_is_the_problem" value="Бактерии">
				</li>
				<li>
					<label for="Jobromania">Биообростания</label>
					<input type="checkbox" id="Jobromania" name="what_is_the_problem" value="Биообростания">
				</li>
			</ul>
			<div class="dms-nav">
				<button type="button" class="hflow_questionnaire-prev">Назад</button>
				<button type="button" class="hflow_questionnaire-next">Продолжить</button>
			</div>
		</div>
	</div>
	<div class="questionnaire-section dms-hidden" >
		<div id="questionnaire-section-03">
			<h3>Диаметр трубопровода подходящего к проблемному участку</h3>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_scale">Накипь</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_scale" name="the_diameter_of_the_piping_approaching_the_problematic_section[scale]" value="" placeholder="d 22 … d 3000 мм">
			</div>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_scale_t">t<sup>O</sup>C воды:</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_scale_t" name="the_diameter_of_the_piping_approaching_the_problematic_section[scale_t]" value="" placeholder="tº22 … tº100">
			</div>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_bacteria">Бактерии</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_bacteria" name="the_diameter_of_the_piping_approaching_the_problematic_section[bacteria]" value="" placeholder="d 22 … d 3000 мм">
			</div>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_bacteria_t">t<sup>O</sup>C воды:</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_bacteria_t" name="the_diameter_of_the_piping_approaching_the_problematic_section[bacteria_t]" value="" placeholder="tº22 … tº100">
			</div>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_jobromania">Жёсткость воды</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_jobromania" name="the_diameter_of_the_piping_approaching_the_problematic_section[jobromania]" value="" placeholder="d 22 … d 3000 мм">
			</div>
			<div class="dms-col-6">
				<label for="the_diameter_of_the_piping_approaching_the_problematic_section_jobromania_t">t<sup>O</sup>C воды:</label>
				<input type="text" id="the_diameter_of_the_piping_approaching_the_problematic_section_jobromania_t" name="the_diameter_of_the_piping_approaching_the_problematic_section[jobromania_t]" value="" placeholder="tº22 … tº100">
			</div>
			<div class="dms-nav">
				<button type="button" class="hflow_questionnaire-prev">Назад</button>
				<button type="button" class="hflow_questionnaire-next">Продолжить</button>
			</div>
		</div>	
	</div>
	<div class="questionnaire-section dms-hidden" >
		<div id="questionnaire-section-04">
			<h3>Какие применяются дезинфицирующие средства?</h3>
			<ul>
				<li>
					<label for="what_sanitizers_are_used_Chlorine">Хлор</label>
					<input type="checkbox" id="what_sanitizers_are_used_Chlorine" name="what_sanitizers_are_used" value="Хлор">
				</li>
				<li>
					<label for="what_sanitizers_are_used_Acid">Кислота</label>
					<input type="checkbox" id="what_sanitizers_are_used_Acid" name="what_sanitizers_are_used" value="Кислота">
				</li>
				<li>
					<label for="what_sanitizers_are_used_Coagulant">Коагулянт</label>
					<input type="checkbox" id="what_sanitizers_are_used_Coagulant" name="what_sanitizers_are_used" value="Коагулянт">
				</li>
			</ul>		
			<div>
				<label for="additional_information">Дополнительная информация:</label>
				<textarea id="additional_information" name="additional_information" placeholder="Дополнительная информация"></textarea>
			</div>
			<div id="contact-date">
				<span><input required name="user[name]" placeholder="*Имя"></span>
				<span><input required name="user[email]" placeholder="*E-mail"></span>
				<span><input required name="user[phone]" placeholder="*Телефон"></span>
			</div>
			<div id="contact-date-2">
				<span><input  name="user[company]" placeholder="Название предприятия"></span>
				<span><input required name="user[field_of_activity]" placeholder="*Сфера деятельности"></span>
			</div>
			<div class="dms-nav">
				<button type="button" class="hflow_questionnaire-prev">Назад</button>
				<button type="button" class="hflow_questionnaire-all">Просмотр</button>
				<button type="supmit" class="hflow_questionnaire-submit">Отправить</button>
			</div>
			<div class="dms-form-report"></div>
			<h2>Все предоставленные данные закрыты для доступа третьих лиц</h2>
		</div>
	</div>
	*|HFLOW_QUESTIONNAIRE_AFTER|*	
</form>
