<?php
/*****************************************************************
Трей содержащий функционал шорткода вывода и обработки формы входа 
для пользователей имеющих профиль
*****************************************************************/
namespace DStaroselskiy\Shortcodes;

if( !class_exists('DStaroselskiy\Shortcodes\SHORTCODE_HFLOW_QUESTIONNAIRE') ) {
	if( !class_exists( '\DStaroselskiy\inc\ADMIN_NOTICES' ) ) require_once( CATALOG_OF_PROFILES_DIR."/include/class_ADMIN_NOTICES.php");

	class SHORTCODE_HFLOW_QUESTIONNAIRE extends \DStaroselskiy\inc\ADMIN_NOTICES {
		protected $form_nonce_fild = "dms-hflow-questionnaire";
		protected $form_nonce_action = "dms-hflow-questionnaire-action";	
		
		public function print_shortcode(){
			if( file_exists( HFLOW_QUESTIONNAIRE_DIR.'tpl/content.tpl' ) ) {
				global $DMS_h_flow_questionnaire;
				$options = $DMS_h_flow_questionnaire->get_options();
				$content = file_get_contents( HFLOW_QUESTIONNAIRE_DIR.'tpl/content.tpl');	
				echo str_replace(
					array(
						'*|WP_NONCE_FIELD|*', 
						'*|HFLOW_QUESTIONNAIRE_BEFORE|*',
						'*||*',
						'*|HFLOW_QUESTIONNAIRE_AFTER|*',
					),
					array(
						\wp_nonce_field( $this->form_nonce_action, $this->form_nonce_fild, true , false ),
						\apply_filters('print_form_hflow_questionnaire_before',''),
						'',
						\apply_filters('print_form_hflow_questionnaire_after',''),
					),
					$content
				);
				\add_action('wp_print_footer_scripts', array( &$this, 'print_in_footer'), 20, 0);
			}
		}
	
		public function print_in_footer(){
		$url = untrailingslashit( HFLOW_QUESTIONNAIRE_URL );
		echo <<<EOF
		<link rel='stylesheet' type='text/css' href='$url/css/core.css'/>		
		<script type='text/javascript' src='$url/js/core.js'></script>
EOF;
		}
		public function	send_mail(){
			if( !isset($_POST[ $this->form_nonce_fild ] ) ) return false;
			if( !wp_verify_nonce($_POST[ $this->form_nonce_fild ], $this->form_nonce_action ) ) {	return false; }
			global $DMS_h_flow_questionnaire;
			$options = $DMS_h_flow_questionnaire->get_options();
			if($options["power_off"] != "true") {
				$this->add_admin_notices( __('Отправка запросов временно отвлючена.' , 'dms_plugin'), 'error', '', false);				
			}else if( file_exists( HFLOW_QUESTIONNAIRE_DIR.'tpl/mail.tpl' ) ) {
				$is_error = false;
				if( !isset($_POST['user']['name']) ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'name', false);
					$is_error = true;
				}else if( $_POST['user']['name'] == "" ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'name', false);
					$is_error = true;
				}
				if( !isset($_POST['user']['phone']) ) {
					$this->add_admin_notices( __('Заполниет номер телефона.' , 'dms_plugin'), 'error', 'phone', false);
					$is_error = true;
				}else if( $_POST['user']['phone'] == "" ) {
					$this->add_admin_notices( __('Заполниет номер телефона.' , 'dms_plugin'), 'error', 'phone', false);
					$is_error = true;
				}
				if( !isset($_POST['user']['email']) ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}else if( $_POST['user']['email'] == "" ) {
					$this->add_admin_notices( __('Заполниет email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}else if( !is_email($_POST['user']['email']) ) {
					$this->add_admin_notices( __('Введите корректный email.' , 'dms_plugin'), 'error', 'email', false);
					$is_error = true;
				}
				if( $is_error ) {
					$this->add_admin_notices( __('Данные не отправлены. Пожалуйста заполните все поля.' , 'dms_plugin'), 'error', '', false);
				}else{					
					
					$body = file_get_contents( HFLOW_QUESTIONNAIRE_DIR.'tpl/mail.tpl');	
					$body = str_replace(
						array(
							'*|MAIL_TITLE|*', 
							'*|MAIL_TITLE_DATE|*',
							'*|USER_NAME|*',
							'*|USER_EMAIL|*',
							'*|USER_PHONE|*',
							'*|COMPANY|*',
							'*|FIELD_OF_ACTIVITY|*',
							'*|the_water_treatment_system|*',
							'*|mechanical_cleaning_other|*',
							'*|daily_consumption_of_cold_water|*',
							'*|the_hardness_of_the_water|*',
							'*|daily_consumption_of_hot_water|*',
							'*|what_is_the_problem|*',
							'*|scale|*',
							'*|scale_t|*',
							'*|bacteria|*',
							'*|bacteria_t|*',
							'*|jobromania|*',
							'*|jobromania_t|*',
							'*|what_sanitizers_are_used|*',
							'*|additional_information|*',
						),
						array(
							$options["title"],
							$options["title"].' от '.date('d-m-Y'),
							esc_html($_POST['user']['name']),
							esc_html($_POST['user']['email']),
							esc_html($_POST['user']['phone']),
							esc_html($_POST['user']['company']),
							esc_html($_POST['user']['field_of_activity']),
							esc_html(is_array($_POST['the_water_treatment_system']) ? implode(",", $_POST['the_water_treatment_system']): $_POST['the_water_treatment_system']),
							esc_html($_POST['mechanical_cleaning_other']),
							esc_html($_POST['daily_consumption_of_cold_water']),
							esc_html($_POST['the_hardness_of_the_water']),
							esc_html($_POST['daily_consumption_of_hot_water']),
							esc_html(is_array($_POST['what_is_the_problem']) ? implode(",", $_POST['what_is_the_problem']): $_POST['what_is_the_problem']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['scale']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['scale_t']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['bacteria']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['bacteria_t']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['jobromania']),
							esc_html($_POST['the_diameter_of_the_piping_approaching_the_problematic_section']['jobromania_t']),
							esc_html(is_array($_POST['what_sanitizers_are_used']) ? implode(",", $_POST['what_sanitizers_are_used']): $_POST['what_sanitizers_are_used']),
							esc_html($_POST['additional_information']),
						),
						$body
					);
					
					$headers = array( 'From: '.$options['send_from'], 'content-type: text/html');
					\add_filter( 'wp_mail_from_name', array( &$this, 'mail_from_name' ) );
					if( wp_mail( $options['send_to'], $options["title"], $body, $headers) ) {
						$this->add_admin_notices( __('Данные отпралены.' , 'dms_plugin'), 'updated', '', false);
						die( json_encode( array( "status" => "ok", "msg" => $this->get_notices() ) ) );
					}else{
						$this->add_admin_notices( __('Ошибка отправки письма. Попробуйте позже.' , 'dms_plugin'), 'error', '', false);
					}
				}
			}else{
				$this->add_admin_notices( __('Файл тела письма не найден. Обратитесь к администраору.' , 'dms_plugin'), 'error', '', false);				
			}
			die( json_encode( array( "status" => "error", "msg" => $this->get_notices() ) ) );
		}
			
		function mail_from_name( $email_from ){
			return get_option('blogname');
		}
		
		function __construct() {
			\add_shortcode('dms-hflow-questionnaire', array( &$this, 'print_shortcode'));
			\add_action('init', array( &$this, 'send_mail'), 1000, 0);
		}
	}

	global $DMS_shortcode_hflow_questionnaire;
	$DMS_shortcode_hflow_questionnaire = new SHORTCODE_HFLOW_QUESTIONNAIRE();
}	
?>