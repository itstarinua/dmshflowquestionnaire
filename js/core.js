(function( $ ) {
	jQuery(document).ready(function(){
		var window_h = jQuery(window).height();
		console.log(window_h);
		jQuery('#hflow_questionnaire .questionnaire-section').css('min-height',window_h);
		
		jQuery('.hflow_questionnaire_show').on('click',function(e){
			e.preventDefault();
			var block_top = jQuery('#hflow_questionnaire').removeClass('dms-hidden').offset();
			jQuery(this).addClass('dms-hidden');
			if( block_top == 'undefined' ) return false;
			if(jQuery.browser.safari || jQuery.browser.chrome){
				jQuery('body').animate( { scrollTop: block_top.top }, 1100 );
			}else{
				jQuery('html').animate( { scrollTop: block_top.top }, 1100 );
			}
			block = jQuery('#hflow_questionnaire .questionnaire-section > div').eq(0);
			if( window_h > block.height() )  block.css("padding-top",( ( window_h - block.height() ) / 2 ) );			
			return false;
		});
		
		jQuery('#hflow_questionnaire .hflow_questionnaire-prev').on('click',function(e){
			e.preventDefault();
			var block_top = jQuery(this).parents('#hflow_questionnaire .questionnaire-section').eq(0).prev().offset();
			if( block_top == 'undefined' ) return false;
			if(jQuery.browser.safari || jQuery.browser.chrome){
				jQuery('body').animate( { scrollTop: block_top.top }, 1100 );
			}else{
				jQuery('html').animate( { scrollTop: block_top.top }, 1100 );
			}
			return false;
		});
		jQuery('#hflow_questionnaire .hflow_questionnaire-next').on('click',function(e){
			e.preventDefault();
			blocks = jQuery(this).parents('#hflow_questionnaire .questionnaire-section').eq(0).next().removeClass('dms-hidden');
			var block_top = blocks.offset();
			if( block_top == 'undefined' ) return false;
			if(jQuery.browser.safari || jQuery.browser.chrome){
				jQuery('body').animate( { scrollTop: block_top.top }, 1100 );
			}else{
				jQuery('html').animate( { scrollTop: block_top.top }, 1100 );
			}
			block = blocks.children().eq(0);
			if( window_h > block.height() ) block.css("padding-top",( ( window_h - block.height() ) / 2 ) );			
			return false;
		});
		
		jQuery('#hflow_questionnaire .hflow_questionnaire-all').on('click',function(e){
			e.preventDefault();
			jQuery('#hflow_questionnaire .hflow_questionnaire-next, #hflow_questionnaire .hflow_questionnaire-prev').remove();
			jQuery('#hflow_questionnaire .questionnaire-section > *').css("padding-top",0).parent().css('min-height',0);
			var block_top = jQuery('#hflow_questionnaire').offset();
			if( block_top == 'undefined' ) return false;
			if(jQuery.browser.safari || jQuery.browser.chrome){
				jQuery('body').animate( { scrollTop: block_top.top }, 1100 );
			}else{
				jQuery('html').animate( { scrollTop: block_top.top }, 1100 );
			}
			jQuery(this).remove();
			return false;
		});
		
		
		jQuery('#hflow_questionnaire .hflow_questionnaire-submit').on('click',function(){
			jQuery(this).addClass("dms-louded").prop('disabled', true);
			var $repport_conteiner = jQuery("#hflow_questionnaire .dms-form-report").empty();
			$form = jQuery(this).parents('#hflow_questionnaire').eq(0);
			// var data = $form.serialize()+"&_wpcf7_is_ajax_call=1";
			var data = $form.serialize();
			jQuery.ajax({ 
				type: 'POST',
				url: '/', 
				dataType: 'json',
				data: data,
				success: function(data){
					data.msg.forEach(function(item){
						$repport_conteiner.append( jQuery('<p class="'+item.status+'">'+item.msg+'</p>') );
					});
					jQuery('#hflow_questionnaire .hflow_questionnaire-submit').removeClass("dms-louded").prop('disabled', false);
				},
				error: function(data){
					$repport_conteiner.append( jQuery('<p class="error">Send error. host is not response</p>') );					
					jQuery('#hflow_questionnaire .hflow_questionnaire-submit').removeClass("dms-louded").prop('disabled', false);
				},
			});			
			return false;
		});
	});
})( jQuery );	